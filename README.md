# To install and run

## Requirements
Python 3.9 

## Get Started

[Install Pyenv](https://github.com/pyenv/pyenv#installation) (optional) If you already have py3.9, or know how to handle it on your own, you can just do it yourself.

[Install Poetry](https://python-poetry.org/docs/#installation)

```
poetry install
poetry shell
python manage.py runserver
```

Then visit `localhost:8000` in your browser.
