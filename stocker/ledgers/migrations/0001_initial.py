# Generated by Django 3.1.7 on 2021-03-09 19:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StockLedger',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku', models.CharField(max_length=100)),
                ('book', models.CharField(max_length=100)),
                ('count', models.IntegerField()),
                ('location', models.CharField(max_length=100)),
                ('condition', models.CharField(choices=[('pristine', 'Pristine'), ('damaged', 'Damaged'), ('destroyed', 'Destroyed')], default='pristine', max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('from_ledger', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='from_transaction', to='ledgers.stockledger')),
                ('to_ledger', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='to_transaction', to='ledgers.stockledger')),
            ],
        ),
    ]
