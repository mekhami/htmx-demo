from django.db import models


class Conditions(models.TextChoices):
    PRISTINE = 'pristine', 'Pristine'
    DAMAGED = 'damaged', 'Damaged'
    DESTROYED = 'destroyed', 'Destroyed'


class Reasons(models.TextChoices):
    ACQUIRED = 'acquired', 'Acquired'
    SOLD = 'sold', 'Sold'
    LOST = 'lost', 'Lost'
    MISSING = 'missing', 'Missing'
    ADJUSTMENT = 'adjustment', 'Adjustment'
    MOVED = 'moved', 'Moved'


class Statuses(models.TextChoices):
    ON_HAND = 'on_hand', 'On Hand'
    OUT = 'out', 'Out'
    SOLD = 'sold', 'Sold'
    MISSING = 'missing', 'Missing'


class Transaction(models.Model):
    from_ledger = models.OneToOneField('StockLedger', on_delete=models.CASCADE, related_name='from_transaction')
    to_ledger = models.OneToOneField('StockLedger', on_delete=models.CASCADE, related_name='to_transaction')
    created = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField()
    reason = models.CharField(max_length=15, choices=Reasons.choices)
    notes = models.TextField(blank=True)

    @property
    def sku(self):
        return self.from_ledger.sku

    @property
    def book(self):
        return self.from_ledger.book


class StockLedgerManager(models.Manager):
    def unique_locations(self, search_term=""):
        return self.filter(location__icontains=search_term).values_list("location", flat=True).distinct()

    def current_ledgers(self):
        return self.filter(is_current=True).exclude(location__in=["VOID", "Missing"])


class StockLedger(models.Model):
    sku = models.CharField(max_length=100, null=True, blank=True)
    book = models.CharField(max_length=100)
    count = models.IntegerField()
    location = models.CharField(max_length=100)
    condition = models.CharField(max_length=15, choices=Conditions.choices, default=Conditions.PRISTINE)
    status = models.CharField(max_length=15, choices=Statuses.choices, default=Statuses.ON_HAND)
    is_current = models.BooleanField(default=False)

    objects = StockLedgerManager()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.sku:
            self.sku = f"book-{self.pk}"
            self.save()
