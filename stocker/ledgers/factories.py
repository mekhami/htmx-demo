import factory
from factory.django import DjangoModelFactory
from django.db import transaction
from faker import Faker
import random
import string

from .models import Transaction, StockLedger, Conditions, Reasons, Statuses

fake = Faker()


class TransactionFactory(DjangoModelFactory):
    class Meta:
        model = Transaction


class StockLedgerFactory(DjangoModelFactory):
    class Meta:
        model = StockLedger


def create_void_ledger(book, sku):
    return StockLedger.objects.create(
        sku=sku,
        book=book,
        count=0,
        location='VOID',
        condition=Conditions.PRISTINE,
    )

@transaction.atomic
def create_adjustment(ledger, new_count, reason, notes):
    ledger.is_current = False
    ledger.save()
    delta = ledger.count - new_count

    to_ledger = StockLedger.objects.create(
        sku=ledger.sku,
        book=ledger.book,
        count=delta,
        status=Statuses.MISSING,
        condition=ledger.condition,
        location='Missing',
        is_current=True
    )

    from_ledger = StockLedger.objects.create(
        sku=ledger.sku,
        book=ledger.book,
        count=new_count,
        status=ledger.status,
        condition=ledger.condition,
        location=ledger.location,
        is_current=True
    )

    return Transaction.objects.create(
        from_ledger=from_ledger,
        to_ledger=to_ledger,
        quantity=delta,
        reason=reason,
        notes=notes
    )


@transaction.atomic
def create_transaction(
    from_ledger,
    quantity,
    destination,
    condition,
    reason="",
    notes=""
):
    to_ledger = StockLedger.objects.create(
        sku=from_ledger.sku,
        book=from_ledger.book,
        count=quantity,
        is_current=True,
        location=destination,
        condition=condition
    )
    from_ledger.is_current = False
    from_ledger.save()
    new_from_ledger = StockLedger.objects.create(
        sku=from_ledger.sku,
        book=from_ledger.book,
        count=from_ledger.count - quantity,
        is_current=True,
        location=from_ledger.location,
        condition=from_ledger.condition
    )
    return Transaction.objects.create(
        from_ledger=from_ledger,
        to_ledger=to_ledger,
        quantity=quantity,
        reason=reason,
        notes=notes
    )


@transaction.atomic
def create_acquisition():
    book = fake.sentence(nb_words=5)
    count = random.choice(range(100))
    condition = Conditions.PRISTINE
    status = 'VOID'

    from_ledger = StockLedger.objects.create(
        book=book,
        count=-count,
        condition=condition,
        status=status,
        location="VOID",
        is_current=False
    )
    to_ledger = StockLedger.objects.create(
        book=book,
        count=count,
        condition=condition,
        status=Statuses.ON_HAND,
        location="Delta-Loading",
        is_current=True
    )
    return Transaction.objects.create(
        from_ledger=from_ledger,
        to_ledger=to_ledger,
        quantity=count,
        reason=Reasons.ACQUIRED,
        notes="Acquired"
    )

@transaction.atomic
def create_random_movement():
    current_ledgers = StockLedger.objects.current_ledgers().filter(count__gt=0)
    from_ledger = random.choice(current_ledgers)
    quantity = random.choice(range(1, from_ledger.count))
    ran_number = lambda: random.choice(string.digits)
    to_ledger = StockLedger.objects.create(
        book=from_ledger.book,
        count=quantity,
        condition=from_ledger.condition,
        status=from_ledger.status,
        location=f"Delta-{ran_number()}-{ran_number()}-{ran_number()}",
        is_current=True
    )
    new_from_ledger = StockLedger.objects.create(
        book=from_ledger.book,
        count=from_ledger.count - quantity,
        condition=from_ledger.condition,
        status=from_ledger.status,
        location=from_ledger.location,
        is_current=True
    )
    from_ledger.is_current = False
    from_ledger.save()
    return Transaction.objects.create(
        from_ledger=from_ledger,
        to_ledger=to_ledger,
        quantity=quantity,
        reason=Reasons.MOVED,
        notes="Random movement"
    )
