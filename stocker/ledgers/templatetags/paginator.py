from django import template

register = template.Library()

@register.inclusion_tag("paginator.html")
def paginator(page_obj):
    return {"page_obj": page_obj}
