from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render
from django.views.generic import View, ListView
import string
import random
from datetime import datetime, timedelta

from .forms import AdjustLedgerForm, MoveLedgerForm
from .models import StockLedger, Transaction, Reasons, Conditions, Statuses
from .factories import create_acquisition, create_random_movement


def home(request):
    return render(request, "ledgers/home.html")

def new_acquisition(request):
    transaction = create_acquisition()
    messages.success(request, f'A new acquisition of {transaction.from_ledger.book} has been recorded.')
    return render(request, "ledgers/new_acquisition.html")

def random_movement(request):
    transaction = create_random_movement()
    messages.success(request, f'A new movement of {transaction.from_ledger.book} has been recorded.')
    return render(request, "ledgers/random_movement.html")

def ledgers(request):
    page = request.GET.get('page', 1)
    book_filter = request.GET.get('book-filter', '')
    condition_filter = request.GET.get('condition-filter', None)

    ledgers = StockLedger.objects.current_ledgers()

    if book_filter:
        ledgers = ledgers.filter(book__icontains=book_filter)
    if condition_filter:
        ledgers = ledgers.filter(condition=condition_filter)

    ledgers = ledgers.all()

    paginator = Paginator(ledgers, 25)
    page_obj = paginator.get_page(page)

    context = {
        "ledgers": ledgers,
        "page_obj": page_obj,
        "book_filter": book_filter,
        "condition_filter": condition_filter,
        "conditions": Conditions.choices,
    }
    if request.htmx:
        template_name = "ledgers/ledgers_partial.html"
    else:
        template_name = "ledgers/ledgers_page.html"
    return render(request, template_name, context)


def adjust_ledger(request, ledger_id):
    ledger = StockLedger.objects.get(id=ledger_id)

    context = {
        "ledger": ledger
    }
    if request.method == "POST":
        form = AdjustLedgerForm(request.POST)
        if form.is_valid():
            form.process(ledger)
            ledgers = StockLedger.objects.current_ledgers()
            paginator = Paginator(ledgers, 25)
            page_obj = paginator.get_page(1)
            context["page_obj"] = page_obj
    else:
        form = AdjustLedgerForm(initial={
            "count": ledger.count
        })

    context["form"] = form
    return render(
        request,
        "ledgers/adjust_modal.html",
        context=context
    )

def move_ledger(request, ledger_id):
    ledger = StockLedger.objects.get(id=ledger_id)

    context = {"ledger": ledger}
    if request.method == "POST":
        form = MoveLedgerForm(request.POST)
        if form.is_valid():
            form.process(ledger)
            ledgers = StockLedger.objects.current_ledgers()
            paginator = Paginator(ledgers, 25)
            page_obj = paginator.get_page(1)
            context["page_obj"] = page_obj
    else:
        form = MoveLedgerForm()

    context["form"] = form

    return render(request, "ledgers/move_modal.html", context=context)

def locations(request):
    search_term = request.GET.get('to_location', '')
    locations = StockLedger.objects.unique_locations(search_term=search_term)
    return render(
        request,
        "ledgers/locations_search.html",
        context={"locations": locations}
    )

def demo_drawer(request):
    closed = True if request.GET.get('close') == "True" else False
    return render(request, 'ledgers/demo_drawer.html', context={'closed': closed})

# Function Based View

def transactions(request):
    page = request.GET.get('page', 1)
    book_filter = request.GET.get('book-filter', '')
    reason_filter = request.GET.get('reason-filter', '')

    transactions = Transaction.objects.filter()
    if book_filter:
        transactions = transactions.filter(from_ledger__book__icontains=book_filter)
    if reason_filter:
        transactions = transactions.filter(reason=reason_filter)

    paginator = Paginator(transactions, 25)
    page_obj = paginator.get_page(page)

    context = {
        'transactions': transactions,
        'page_obj': page_obj,
        'book_filter': book_filter,
        'reason_filter': reason_filter,
        'reasons': Reasons.choices
    }

    if request.htmx:
        template_name = "ledgers/transactions_partial.html"
    else:
        template_name = "ledgers/transactions_page.html"
    return render(request, template_name, context)


# Helper Mixin for HTMX requests; could add more here, like logging,
# debug information. The 'request.htmx' comes from github.com/adamchainz/django-htmx,
# from one of django's core maintainers. it has a few other helpful middleware-enabled
# properties like `current_url`, `target`, `trigger`, etc. Can potentially be useful
# in determining how to render the response. Here, we just use it to say, if
# the request is an htmx ajax request, render the partial. If it's a fully qualified
# request from the browser, render the whole page.

class HtmxMixin(View):
    def get_template_names(self):
        if self.request.htmx:
            template = self.partial_name
        else:
            template = self.template_name
        return [template]

class TransactionsView(HtmxMixin, ListView):
    partial_name = "ledgers/transactions_partial.html"
    template_name = "ledgers/transactions_page.html"
    paginate_by = 25
    model = Transaction
    context_object_name = "transactions"

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.book_filter = request.GET.get('book-filter', '')
        self.reason_filter = request.GET.get('reason-filter', '')

    def get_queryset(self):
        transactions = Transaction.objects.select_related('from_ledger', 'to_ledger').order_by('-created')
        if self.book_filter:
            transactions = transactions.filter(from_ledger__book__icontains=self.book_filter)
        if self.reason_filter:
            transactions = transactions.filter(reason=self.reason_filter)
        return transactions

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'reasons': Reasons.choices,
            'book_filter': self.book_filter,
            'reason_filter': self.reason_filter
        })
        return context
