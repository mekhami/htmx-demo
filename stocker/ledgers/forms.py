from django import forms
from .models import Reasons
from .factories import create_transaction, create_adjustment


class AdjustLedgerForm(forms.Form):
    count = forms.IntegerField(min_value=0)
    reason = forms.ChoiceField(choices=Reasons.choices)
    notes = forms.CharField(required=False)

    def process(self, ledger):
        transaction = create_adjustment(
            ledger,
            self.cleaned_data["count"],
            self.cleaned_data["reason"],
            self.cleaned_data["notes"]
        )
        return transaction


class MoveLedgerForm(forms.Form):
    quantity = forms.IntegerField(min_value=0)
    to_location = forms.CharField(initial="")

    def process(self, ledger):
        transaction = create_transaction(
            ledger,
            self.cleaned_data["quantity"],
            self.cleaned_data["to_location"],
            ledger.condition,
            reason="Moved"
        )
        return transaction
