from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('new-acquisition', views.new_acquisition),
    path('random-movement', views.random_movement),
    path('ledgers', views.ledgers),
    path('transactions', views.TransactionsView.as_view()),
    path('adjust-ledger/<int:ledger_id>', views.adjust_ledger),
    path('move-ledger/<int:ledger_id>', views.move_ledger),
    path('search-locations', views.locations),
    path('demo-drawer', views.demo_drawer),
]
